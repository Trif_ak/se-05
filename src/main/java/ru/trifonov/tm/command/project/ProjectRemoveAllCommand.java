package ru.trifonov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class ProjectRemoveAllCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-removeAllOfProject";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": removeOne all projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT REMOVE ALL]");
        @Nullable final String userId = serviceLocator.getCurrentUserID();
        serviceLocator.getTaskService().removeAllOfUser(userId);
        serviceLocator.getProjectService().removeAll(userId);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};

    }
}
