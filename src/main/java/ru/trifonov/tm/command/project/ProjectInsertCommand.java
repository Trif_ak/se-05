package ru.trifonov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class ProjectInsertCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-insert";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": create new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT INSERT]");
        @Nullable final String userId = serviceLocator.getCurrentUserID();
        System.out.println("Enter name");
        @Nullable final String name = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter description");
        @Nullable final String description = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter start date. Date format DD.MM.YYYY");
        @Nullable final String beginDate = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter finish date. Date format DD.MM.YYYY");
        @Nullable final String endDate = serviceLocator.getInCommand().nextLine();
        serviceLocator.getProjectService().insert(name, userId, description, beginDate, endDate);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};

    }
}
