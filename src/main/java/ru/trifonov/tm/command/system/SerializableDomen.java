package ru.trifonov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class SerializableDomen extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "serializable-domain";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": serializable domain";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DOMAIN SERIALIZABLE]");
        @NotNull final FileOutputStream projectOut = new FileOutputStream("src\\main\\files\\ProjectFile");
        @NotNull final ObjectOutputStream projectObjectOut = new ObjectOutputStream(projectOut);
        projectObjectOut.writeObject(serviceLocator.getProjectService().findAll());
        @NotNull final FileOutputStream taskOut = new FileOutputStream("src\\main\\files\\TaskFile");
        @NotNull final ObjectOutputStream taskObjectOut = new ObjectOutputStream(taskOut);
        taskObjectOut.writeObject(serviceLocator.getTaskService().findAll());
        @NotNull final FileOutputStream userOut = new FileOutputStream("src\\main\\files\\UserFile");
        @NotNull final ObjectOutputStream userObjectOut = new ObjectOutputStream(userOut);
        userObjectOut.writeObject(serviceLocator.getUserService().findAll());
//        @Nullable final String userId = serviceLocator.getCurrentUserID();
//        System.out.println("Enter the ID of the project you want to removeOne");
//        @Nullable final String id = serviceLocator.getInCommand().nextLine();
//        serviceLocator.getTaskService().removeAllOfProject(id, userId);
//        serviceLocator.getProjectService().removeOne(id, userId);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.ADMIN};

    }
}
