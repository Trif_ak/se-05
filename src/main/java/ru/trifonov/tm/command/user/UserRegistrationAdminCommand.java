package ru.trifonov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class UserRegistrationAdminCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "user-regAdmin";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": registration new admin";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER REGISTRATION]");
        System.out.println("Enter your LOGIN");
        @Nullable final String login = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter your PASSWORD");
        @Nullable final String password = serviceLocator.getInCommand().nextLine();
        System.out.println("ADMIN " + serviceLocator.getUserService().registrationAdmin(login, password) + " is registered");
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.ADMIN};
    }
}
