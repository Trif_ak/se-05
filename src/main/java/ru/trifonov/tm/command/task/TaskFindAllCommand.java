package ru.trifonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.entity.Task;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.Collection;

public final class TaskFindAllCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-findAll";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": return all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK FIND ALL]");
        @Nullable final String userId = serviceLocator.getCurrentUserID();
        System.out.println("Enter ID of project");
        @Nullable final String projectId = serviceLocator.getInCommand().nextLine();
        @NotNull final Collection<Task> inputList = serviceLocator.getTaskService().findAll(projectId, userId);
        for (@NotNull final Task task : inputList) {
            System.out.println(task);
        }
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
