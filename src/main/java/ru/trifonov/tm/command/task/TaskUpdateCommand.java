package ru.trifonov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.command.AbstractCommand;
import ru.trifonov.tm.enumerate.RoleType;

public final class TaskUpdateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return ": update select task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK UPDATE]");
        @Nullable final String userId = serviceLocator.getCurrentUserID();
        System.out.println("Enter the ID of the task you want to update");
        @Nullable final String id = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new name");
        @Nullable final String name = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new projectId");
        @Nullable final String projectId = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new description");
        @Nullable final String description = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new start date task. Date format DD.MM.YYYY");
        @Nullable final String beginDate = serviceLocator.getInCommand().nextLine();
        System.out.println("Enter new finish date task. Date format DD.MM.YYYY");
        @Nullable final String endDate = serviceLocator.getInCommand().nextLine();
        serviceLocator.getTaskService().update(name, id, projectId, userId, description, beginDate, endDate);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public RoleType[] roleType() {
        return new RoleType[] {RoleType.REGULAR_USER, RoleType.ADMIN};
    }
}
