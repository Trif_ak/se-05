package ru.trifonov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.trifonov.tm.entity.User;
import ru.trifonov.tm.enumerate.RoleType;

import java.util.Collection;
import java.util.List;

public interface IUserRepository {
    void persist(@NotNull User user);
    void merge(@NotNull User user);
    void insert(@NotNull String id, @NotNull String login, @NotNull String password, @NotNull RoleType roleType);
    void update(@NotNull String id, @NotNull String login, @NotNull String password, @NotNull RoleType roleType);
    User findOne(@NotNull String id) throws Exception;
    List<User> findAll() throws Exception;
    User findLogin(@NotNull String login) throws Exception;
    User findPassword(@NotNull User user, @NotNull String PasswordMD5) throws Exception;
    void removeOne(@NotNull String id);
    void removeAll();
}
