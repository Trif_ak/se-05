<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>PROJECTS</title>
</head>
<body>

<h2>Films</h2>
<table>
    <tr>
        <th>id</th>
        <th>title</th>
        <th>year</th>
        <th>genre</th>
        <th>watched</th>
        <th>action</th>
    </tr>
    <c:forEach var="project" items="${projectList}">
        <tr>
            <td>${project.id}</td>
            <td>${project.name}</td>
            <td>${project.description}</td>
<%--            <td>--%>
<%--                <a href="/edit/${film.id}">edit</a>--%>
<%--                <a href="/delete/${film.id}">delete</a>--%>
<%--            </td>--%>
        </tr>
    </c:forEach>
</table>
<%--<h2>Add</h2>--%>
<%--<c:url value="/add" var="add"/>--%>
<%--<a href="${add}">Add new film</a>--%>
</body>
</html>