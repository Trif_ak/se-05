package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.trifonov.tm.api.ITaskService;
import ru.trifonov.tm.model.Task;
import ru.trifonov.tm.repository.ITaskRepository;

import java.util.List;

public final class TaskService implements ITaskService {
    @NotNull
    @Autowired
    ITaskRepository taskRepository;

    @Override
    public void persist(@Nullable final Task task) {
        if (task == null) throw new NullPointerException("Enter correct data.");
        taskRepository.save(task);
    }

    @Override
    public void update(
            @Nullable final String id, @Nullable final String name, @Nullable final String description
    ) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct name.");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct description.");
        taskRepository.update(id, name, description);
    }

    @Override
    public List<Task> findAll() {
        @Nullable List<Task> tasks = (List<Task>) taskRepository.findAll();
        if (tasks.isEmpty()) throw new NullPointerException("Tasks not found.");
        return tasks;
    }

    @Override
    public Task find(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        if (!taskRepository.findById(id).isPresent()) throw new NullPointerException("Task not found.");
        @Nullable Task task = taskRepository.findById(id).get();
        return task;
    }

    @Override
    public void delete(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        taskRepository.deleteById(id);
    }
}
