package ru.trifonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.trifonov.tm.api.IProjectService;
import ru.trifonov.tm.model.Project;
import ru.trifonov.tm.repository.IProjectRepository;

import java.util.List;

public final class ProjectService implements IProjectService {
    @NotNull
    @Autowired
    IProjectRepository projectRepository;

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) throw new NullPointerException("Enter correct data.");
        projectRepository.save(project);
    }

    @Override
    public void update(
            @Nullable final String id, @Nullable final String name, @Nullable final String description
    ) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        if (name == null || name.trim().isEmpty()) throw new NullPointerException("Enter correct name.");
        if (description == null || description.trim().isEmpty()) throw new NullPointerException("Enter correct description.");
        projectRepository.update(id, name, description);
    }

    @Override
    public List<Project> findAll() {
        @Nullable List<Project> projects = (List<Project>) projectRepository.findAll();
        if (projects.isEmpty()) throw new NullPointerException("Projects not found.");
        return projects;
    }

    @Override
    public Project find(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        if (!projectRepository.findById(id).isPresent()) throw new NullPointerException("Project not found.");
        @Nullable Project project = projectRepository.findById(id).get();
        return project;
    }

    @Override
    public void delete(@Nullable final String id) {
        if (id == null || id.trim().isEmpty()) throw new NullPointerException("Enter correct id.");
        projectRepository.deleteById(id);
    }
}
