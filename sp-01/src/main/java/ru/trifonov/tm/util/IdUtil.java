package ru.trifonov.tm.util;

import lombok.experimental.UtilityClass;

import java.util.UUID;

@UtilityClass
public class IdUtil {
    public static String getUUID() {
        return UUID.randomUUID().toString();
    }

}
