package ru.trifonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.trifonov.tm.model.Task;

@Repository
public interface ITaskRepository extends CrudRepository<Task, String> {
    @Query("")
    void update(@NotNull final String id, @NotNull final String name, @NotNull final String description);
}
