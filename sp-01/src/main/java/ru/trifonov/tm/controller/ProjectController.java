package ru.trifonov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.trifonov.tm.api.IProjectService;
import ru.trifonov.tm.model.Project;

import java.util.List;

@Controller
public class ProjectController {
    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @GetMapping("/projects")
    public ModelAndView projectList (final Model model) {
        @Nullable List<Project> projects = projectService.findAll();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("projects");
        modelAndView.addObject("projectList", projects);
        return modelAndView;
    }
}
