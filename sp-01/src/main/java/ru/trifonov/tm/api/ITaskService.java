package ru.trifonov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.model.Task;

import java.util.List;

public interface ITaskService {
    void persist(@Nullable Task task);

    void update(
            @Nullable String id, @Nullable String name, @Nullable String description
    );

    List<Task> findAll();

    Task find(@Nullable String id);

    void delete(@Nullable String id);
}
