package ru.trifonov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.trifonov.tm.model.Project;

import java.util.List;

public interface IProjectService {
    void persist(@Nullable Project project);

    void update(
            @Nullable String id, @Nullable String name, @Nullable String description
    );

    List<Project> findAll();

    Project find(@Nullable String id);

    void delete(@Nullable String id);
}
